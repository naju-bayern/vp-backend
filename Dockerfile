FROM centos:7

RUN  yum -y update && yum install -y epel-release && yum -y update && yum -y install\ 
        bash \
        golang \
        qpdf \
        wkhtmltopdf \
        xorg-x11-server-Xvfb \
        && yum clean all
COPY ./ /srv
ENTRYPOINT ["go", "run", "/srv/server.go"]

