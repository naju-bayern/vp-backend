package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
)

// TODO create global map with pageSizes
type pageSize struct {
	displayID   string
	resolution  string
	name        string
	orientation string
}
type page struct {
	URL  string
	Size string
}

type task struct {
	//	Pdftitle, Pdfauthor, Pdfsubject, Pdfkeywords string
	Pages []page
	//	DPI                                          int //TODO Add support

}

var pageSizes = map[string]pageSize{
	"a3potrait": pageSize{
		":0", "4961x3508x24+32", "A3", "Potrait",
	},
	"a3landscape": pageSize{
		":1", "3508x4961x24+32", "A3", "Landscape",
	},
	"a4potrait": pageSize{
		":2", "3508x2480x24+32", "A4", "Potrait",
	},
	"a4landscape": pageSize{
		":3", "2480x3508x24+32", "A4", "Landscape",
	},
	"a5potrait": pageSize{
		":4", "2480x1748x24+32", "A5", "Potrait",
	},
	"a5landscape": pageSize{
		":5", "1748x2480x24+32", "A5", "Landscape",
	},
	"a6potrait": pageSize{
		":6", "1748x1240x24+32", "A6", "Potrait",
	},
	"a6landscape": pageSize{
		":7", "1240x1748x24+32", "A6", "Landscape",
	},
}

func main() {
	initialize()
	http.HandleFunc("/", generateVP)
	http.ListenAndServe(":8080", nil)
}

func initialize() {
	for _, s := range pageSizes {
		go runVirtualXServer(s)
	}
}

func generateVP(rw http.ResponseWriter, r *http.Request) {
	tmpdir, err := ioutil.TempDir("", "vp-backend")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)
	var task task
	if r.Body == nil {

		http.Error(rw, "Invalid Request. No request body provided", 400)
		return

	}
	err = json.NewDecoder(r.Body).Decode(&task)
	if err != nil {
		http.Error(rw, err.Error(), 400)
		return
	}

	fmt.Println("Decoding worked")
	fmt.Println(task)
	for i, page := range task.Pages {
		generatepage(page, path.Join(tmpdir, fmt.Sprintf("page%04d.pdf", i)))
	}
	assemblepages(task, tmpdir)

	http.ServeFile(rw, r, path.Join(tmpdir, "vp.pdf"))
	fmt.Println(tmpdir)
	files, err := filepath.Glob(tmpdir + "/*")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(files)
}

func runVirtualXServer(size pageSize) {
	err := exec.Command("Xvfb", size.displayID, "-screen", "0", size.resolution, "-dpi", "240").Run()
	// We should never reach that code
	fmt.Fprintln(os.Stderr, "Xvfb stopped unexpectedly")
	log.Fatal(err)

}

func generatepage(page page, target string) error {
	fmt.Println("Called generatePage " + target)
	os.Setenv("DISPLAY", pageSizes[page.Size].displayID)
	//Possible security bug: wkhtmltopdf might allow command injection
	out, err := exec.Command("wkhtmltopdf",
		"--no-stop-slow-scripts",
		"-s", pageSizes[page.Size].name,
		"-d", "300",
		"--margin-top", "0",
		"--margin-bottom", "0",
		"--margin-right", "0",
		"--margin-left", "0",
		page.URL,
		target).CombinedOutput()
	fmt.Println(string(out))
	return err
}

func assemblepages(task task, tmpdir string) error {
	fmt.Println("Called Assemblepages")
	files, err := filepath.Glob(tmpdir + "/page*.pdf")
	if err != nil {
		log.Fatal(err)
	}
	args := append([]string{"--empty", "--pages"},
		append(files, []string{"--", path.Join(tmpdir, "vp.pdf")}...)...)
	fmt.Println(args)
	// Possible security bug: pdfjam might allow command injection
	cmd := exec.Command("qpdf", args...)
	//cmd := exec.Command("echo", "foo")
	out, errc := cmd.CombinedOutput()
	fmt.Println(string(out))
	fmt.Println(err)
	return errc
}
